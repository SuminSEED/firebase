FROM node:14.17.6-stretch

RUN apt update

RUN apt install -y default-jre-headless imagemagick

RUN npm config -g set user root

RUN npm update -g

RUN npm install -g firebase-tools@9.18.0

ENTRYPOINT ["firebase"]

CMD ["--help"]